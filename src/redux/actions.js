import { PUT_ITEM, REMOVE_ITEM } from 'redux/types';

export const putItem = (item) => ({
  type: PUT_ITEM,
  payload: putItem,
});

export const removeItem = () => ({
  type: REMOVE_ITEM,
  payload: removeItem,
});
